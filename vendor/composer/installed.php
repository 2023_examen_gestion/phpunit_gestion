<?php return array(
    'root' => array(
        'pretty_version' => '1.0',
        'version' => '1.0.0.0',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => NULL,
        'name' => '__root__',
        'dev' => true,
    ),
    'versions' => array(
        '__root__' => array(
            'pretty_version' => '1.0',
            'version' => '1.0.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => NULL,
            'dev_requirement' => false,
        ),
        'doctrine/instantiator' => array(
            'pretty_version' => '1.3.1',
            'version' => '1.3.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/instantiator',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'guzzlehttp/guzzle' => array(
            'pretty_version' => '7.5.0',
            'version' => '7.5.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../guzzlehttp/guzzle',
            'aliases' => array(),
            'reference' => 'b50a2a1251152e43f6a37f0fa053e730a67d25ba',
            'dev_requirement' => false,
        ),
        'guzzlehttp/promises' => array(
            'pretty_version' => '1.5.2',
            'version' => '1.5.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../guzzlehttp/promises',
            'aliases' => array(),
            'reference' => 'b94b2807d85443f9719887892882d0329d1e2598',
            'dev_requirement' => false,
        ),
        'guzzlehttp/psr7' => array(
            'pretty_version' => '2.4.3',
            'version' => '2.4.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../guzzlehttp/psr7',
            'aliases' => array(),
            'reference' => '67c26b443f348a51926030c83481b85718457d3d',
            'dev_requirement' => false,
        ),
        'myclabs/deep-copy' => array(
            'pretty_version' => '1.11.0',
            'version' => '1.11.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../myclabs/deep-copy',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'nikic/php-parser' => array(
            'pretty_version' => 'v4.15.3',
            'version' => '4.15.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nikic/php-parser',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'phar-io/manifest' => array(
            'pretty_version' => '2.0.3',
            'version' => '2.0.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phar-io/manifest',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'phar-io/version' => array(
            'pretty_version' => '3.2.1',
            'version' => '3.2.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phar-io/version',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'phpmailer/phpmailer' => array(
            'pretty_version' => 'v6.6.0',
            'version' => '6.6.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpmailer/phpmailer',
            'aliases' => array(),
            'reference' => 'e43bac82edc26ca04b36143a48bde1c051cfd5b1',
            'dev_requirement' => false,
        ),
        'phpunit/php-code-coverage' => array(
            'pretty_version' => '9.2.23',
            'version' => '9.2.23.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpunit/php-code-coverage',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'phpunit/php-file-iterator' => array(
            'pretty_version' => '3.0.6',
            'version' => '3.0.6.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpunit/php-file-iterator',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'phpunit/php-invoker' => array(
            'pretty_version' => '3.1.1',
            'version' => '3.1.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpunit/php-invoker',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'phpunit/php-text-template' => array(
            'pretty_version' => '2.0.4',
            'version' => '2.0.4.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpunit/php-text-template',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'phpunit/php-timer' => array(
            'pretty_version' => '5.0.3',
            'version' => '5.0.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpunit/php-timer',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'phpunit/phpunit' => array(
            'pretty_version' => '9.6.3',
            'version' => '9.6.3.0',
            'reference' => 'e7b1615e3e887d6c719121c6d4a44b0ab9645555',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpunit/phpunit',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'psr/http-client' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/http-client',
            'aliases' => array(),
            'reference' => '2dfb5f6c5eff0e91e20e913f8c5452ed95b86621',
            'dev_requirement' => false,
        ),
        'psr/http-client-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'psr/http-factory' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/http-factory',
            'aliases' => array(),
            'reference' => '12ac7fcd07e5b077433f5f2bee95b3a771bf61be',
            'dev_requirement' => false,
        ),
        'psr/http-factory-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'psr/http-message' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/http-message',
            'aliases' => array(),
            'reference' => 'f6561bf28d520154e4b0ec72be95418abe6d9363',
            'dev_requirement' => false,
        ),
        'psr/http-message-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'ralouphie/getallheaders' => array(
            'pretty_version' => '3.0.3',
            'version' => '3.0.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../ralouphie/getallheaders',
            'aliases' => array(),
            'reference' => '120b605dfeb996808c31b6477290a714d356e822',
            'dev_requirement' => false,
        ),
        'sebastian/cli-parser' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/cli-parser',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'sebastian/code-unit' => array(
            'pretty_version' => '1.0.8',
            'version' => '1.0.8.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/code-unit',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'sebastian/code-unit-reverse-lookup' => array(
            'pretty_version' => '2.0.3',
            'version' => '2.0.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/code-unit-reverse-lookup',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'sebastian/comparator' => array(
            'pretty_version' => '4.0.8',
            'version' => '4.0.8.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/comparator',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'sebastian/complexity' => array(
            'pretty_version' => '2.0.2',
            'version' => '2.0.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/complexity',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'sebastian/diff' => array(
            'pretty_version' => '4.0.4',
            'version' => '4.0.4.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/diff',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'sebastian/environment' => array(
            'pretty_version' => '5.1.4',
            'version' => '5.1.4.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/environment',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'sebastian/exporter' => array(
            'pretty_version' => '4.0.5',
            'version' => '4.0.5.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/exporter',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'sebastian/global-state' => array(
            'pretty_version' => '5.0.5',
            'version' => '5.0.5.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/global-state',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'sebastian/lines-of-code' => array(
            'pretty_version' => '1.0.3',
            'version' => '1.0.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/lines-of-code',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'sebastian/object-enumerator' => array(
            'pretty_version' => '4.0.4',
            'version' => '4.0.4.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/object-enumerator',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'sebastian/object-reflector' => array(
            'pretty_version' => '2.0.4',
            'version' => '2.0.4.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/object-reflector',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'sebastian/recursion-context' => array(
            'pretty_version' => '4.0.4',
            'version' => '4.0.4.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/recursion-context',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'sebastian/resource-operations' => array(
            'pretty_version' => '3.0.3',
            'version' => '3.0.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/resource-operations',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'sebastian/type' => array(
            'pretty_version' => '3.2.0',
            'version' => '3.2.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/type',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'sebastian/version' => array(
            'pretty_version' => '3.0.2',
            'version' => '3.0.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../sebastian/version',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/deprecation-contracts' => array(
            'pretty_version' => 'v3.2.0',
            'version' => '3.2.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/deprecation-contracts',
            'aliases' => array(),
            'reference' => '1ee04c65529dea5d8744774d474e7cbd2f1206d3',
            'dev_requirement' => false,
        ),
        'theseer/tokenizer' => array(
            'pretty_version' => '1.2.1',
            'version' => '1.2.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../theseer/tokenizer',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
